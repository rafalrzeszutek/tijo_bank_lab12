package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class Deposit extends Specification{

    @Unroll
    def "should deposit #depositMoney cash to account number #accountNumber"(){
        given: "initial data"
        def accounts = Bank.bankAccountsList
        def bank = new Bank()
        bank.addBankAccounts(accounts)
        when: "deposit cash to account"
        def result = bank.deposit(accountNumber, depositMoney)
        then: "check if successful"
        result

        where:
        accountNumber | depositMoney
        1       |       500
        2       |       650
        3       |       500
        4       |       900
    }
}
