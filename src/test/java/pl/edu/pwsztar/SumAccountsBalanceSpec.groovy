package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class SumAccountsBalanceSpec extends Specification{

    @Unroll
    def "should add money and return 9360 from all accounts"(){
        given: "Initial data"
        def bank = new Bank()
        def accounts = Bank.bankAccountsList
        bank.addBankAccounts(accounts)
        when: "sum money from all accounts"
        def result = bank.sumAccountsBalance()
        then: "check sum"

        result == 9360
    }
}
