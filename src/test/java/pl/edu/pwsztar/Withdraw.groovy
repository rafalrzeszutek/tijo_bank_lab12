package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class Withdraw extends Specification{

    @Unroll
    def "should withdraw #withdrawMoney of #money from account #accountNumber"(){
        given: "initial data"
        def accounts = Bank.bankAccountsList
        def bank = new Bank()
        bank.addBankAccounts(accounts)

        when: "withdraw money"
        def result = bank.withdraw(accountNumber, withdrawMoney)
        then: "check if successful"
        result

        where:
        accountNumber |  money      | withdrawMoney
        1             |  3000       | 50
        2             |  1000       | 50
        3             |  500        | 50
        4             |  4860       | 50
    }
}
