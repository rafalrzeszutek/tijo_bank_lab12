package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification{

    @Unroll
    def "should delete account number #accountNumber with cash #accountMoney"(){
        given: "initial data"
        def bank = new Bank()
        def accounts = Bank.bankAccountsList
        bank.addBankAccounts(accounts)

        when: "account deleted"
        def cash = bank.deleteAccount(accountNumber)
        then: "check deleted account money"
        cash == accountMoney

        where:
        accountNumber | accountMoney
        1   | 3000
        2   | 1000
        3   | 500
        4   | 4860
    }
}
