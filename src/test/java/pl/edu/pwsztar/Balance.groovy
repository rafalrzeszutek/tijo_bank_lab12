package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class Balance extends Specification{

    @Unroll
    def "should return #cash cash for #account account"(){
        given: "Initial data"
        def bank = new Bank()
        def accounts = Bank.bankAccountsList
        bank.addBankAccounts(accounts)
        when: "get account balance"
        def balance = bank.accountBalance(account)
        then: "check balance"
        balance == cash

        where:
        account | cash
        1   | 3000
        2   | 1000
        3   | 500
        4   | 4860
    }
}
