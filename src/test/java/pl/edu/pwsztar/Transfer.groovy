package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class Transfer extends Specification{

    @Unroll
    def "should transfer from #fromAccount to #toAccount account #money cash"(){
        given: "initial data"
        def bank = new Bank()
        def accounts = Bank.bankAccountsList
        bank.addBankAccounts(accounts)
        when: "transfer money"
        def result = bank.transfer(fromAccount, toAccount, money)
        then: "check if transfer is successful"
        result

        where:
        fromAccount   |   toAccount   | money
        1             |       2       | 50
        2             |       3       | 50
        3             |       4       | 50

    }
}
