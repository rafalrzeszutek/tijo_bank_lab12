package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// TODO: Prosze dokonczyc implementacje oraz testy jednostkowe
// TODO: Prosze nie zmieniac nazw metod - wszystkie inne chwyty dozwolone
// TODO: (prosze jedynie trzymac sie dokumentacji zawartej w interfejsie BankOperation)
class Bank implements BankOperation {

    private static int accountNumber = 0;
    private final List<BankAccount> accounts_list = new ArrayList<>();

    public int createAccount() {
        accounts_list.add(new BankAccount(0, ++accountNumber));

        return accountNumber;
    }

    public int deleteAccount(int accountNumber) {
        int balance = ACCOUNT_NOT_EXISTS;

        BankAccount bank_account = findBankAccount(accountNumber)
                                    .orElse( new BankAccount(0,ACCOUNT_NOT_EXISTS));

        if (bank_account.getAccountNumber() != ACCOUNT_NOT_EXISTS) {
            balance = bank_account.getBalance();
            accounts_list.remove(bank_account);
            Bank.accountNumber--;
        }
        return balance;
    }

    public boolean deposit(int accountNumber, int amount) {
        BankAccount bank_account = findBankAccount(accountNumber)
                                    .orElse( new BankAccount(0,ACCOUNT_NOT_EXISTS));

        if(bank_account.getAccountNumber() != ACCOUNT_NOT_EXISTS && amount > 0){
            bank_account.setBalance(bank_account.getBalance() + amount);
            return true;
        }

        return false;
    }

    public boolean withdraw(int accountNumber, int amount) {
        if(amount <= 0){
            return false;
        }

        BankAccount bank_account = findBankAccount(accountNumber).orElse( new BankAccount(0,ACCOUNT_NOT_EXISTS));

        if(bank_account.getAccountNumber() != ACCOUNT_NOT_EXISTS && bank_account.getBalance() >= amount){
            bank_account.setBalance(bank_account.getBalance() - amount);
            return true;
        }

        return false;
    }

    public boolean transfer(int from, int to, int amount) {
        if(from == to || amount <= 0){
            return false;
        }

        BankAccount fromAcc = findBankAccount(from).orElse( new BankAccount(0,ACCOUNT_NOT_EXISTS));
        BankAccount toAcc = findBankAccount(from).orElse( new BankAccount(0,ACCOUNT_NOT_EXISTS));

        if(fromAcc.getAccountNumber() != ACCOUNT_NOT_EXISTS && toAcc.getAccountNumber() != ACCOUNT_NOT_EXISTS
                && fromAcc.getBalance() >= amount){

            fromAcc.setBalance(fromAcc.getBalance() - amount);
            toAcc.setBalance(toAcc.getBalance() + amount);

            return true;
        }

        return false;
    }

    public int accountBalance(int accountNumber) {
        BankAccount account = findBankAccount(accountNumber).orElse( new BankAccount(ACCOUNT_NOT_EXISTS,0));

        if(account.getAccountNumber() != ACCOUNT_NOT_EXISTS){
            return account.getBalance();
        }

        return ACCOUNT_NOT_EXISTS;
    }

    public int sumAccountsBalance() {
        return accounts_list.stream().mapToInt(BankAccount::getBalance).sum();
    }


    public static List<BankAccount> getBankAccountsList(){
        List<BankAccount> accounts_list = new ArrayList<>();
        accounts_list.add(new BankAccount(3000, 1));
        accounts_list.add(new BankAccount(1000, 2));
        accounts_list.add(new BankAccount(500, 3));
        accounts_list.add(new BankAccount(4860, 4));

        return accounts_list;
    }

    private void addBankAccounts(List<BankAccount> acc) {
        accounts_list.addAll(acc);
        accountNumber = acc.size();
    }

    private Optional<BankAccount> findBankAccount(int accountNumber){
        for(BankAccount account : accounts_list){
            if(account.getAccountNumber() == accountNumber){
                return Optional.of(account);
            }
        }

        return Optional.empty();
    }
}
